export { CheckComponent } from './auth/components/check/check.component';
export { LoginPageComponent } from './auth/components/login-page/login-page.component';

export { AuthModule } from './auth/auth.module';
export { AuthRoutingModule } from './auth/auth-routing.module'
export { AuthGuard } from './auth/auth.guard'
export { AuthService } from './auth/auth.service'