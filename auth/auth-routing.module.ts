import { CheckComponent } from './components/check/check.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { LogoutComponent } from './components/logout/logout.component'


const routes: Routes = [
	{
		path: '', children: [
			{ path: 'check', component: CheckComponent },
			{ path: 'login', component: LoginPageComponent },
			{ path: 'logout', component: LogoutComponent }
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AuthRoutingModule { }
