import { Router, ActivatedRoute, Params } from '@angular/router';
import { Injectable, OnDestroy } from '@angular/core';
import { RequestOptions, Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService implements OnDestroy {

	public token: string;
	public sistemaKey: string;
	public redirectUrl: string;
	public sistema: any;
	public localStorageId: string = 'jwt'
	public ssoUrl: string;
	private routeSubscription;

	constructor(private http: Http, private router: Router, private route: ActivatedRoute) {
		// Fica de olho nos parâmetros da url para login, check etc...
		this.routeSubscription = this.route.queryParams.subscribe((params: Params) => {
			this.redirectUrl = params['redirect-url'];
			this.sistemaKey = params['sistema-key'];
		})
		// Pega o token do localStorage e joga na memória
		const currentUser = JSON.parse(localStorage.getItem(this.localStorageId));
		this.token = currentUser && currentUser.token;
	}
	setToken(token, user = null) {
		this.token = token;
		localStorage.setItem(this.localStorageId, JSON.stringify({ user: user, token: token }));
	}

	login(username: string, password: string): Observable<boolean> {
		return this.http.post(this.ssoUrl + '/login', { username, password, sistemaKey: this.sistemaKey })
			.map((response: Response) => {
				// Da bom se tver um token na resposta
				const resposta = response.json();

				const token = resposta.token;
				if (token) {
					// seta o token
					// Salva o usuário no localStorage pra persistir entre abas e paginas
					this.setToken(token, username)
					// salva os dados do sistema para dar redirect
					this.sistema = resposta.sistema;
					if (this.redirectUrl) {
						this.redirectSetToken()
					}
					// Retorna true pra indicar sucesso
					return true;
				} else {
					// Retorna falso pra indicar falso login
					return false;
				}
			});
	}

	validarToken() {
		const headers = new Headers({ 'Authorization': 'Bearer ' + this.token });
		const options = new RequestOptions({ headers: headers });
		const body = {
			sistemaKey: this.sistemaKey
		};
		// retorna o usuário logado ou false
		return this.http.post(this.ssoUrl + '/check-user', body, options)
			.map((response: Response) => {
				// Da bom se tver um token na resposta
				const resposta = response.json();

				const token = resposta.token;
				if (token) {
					// seta o token
					this.token = token;
					this.sistema = resposta.sistema;
					// Salva o usuário no localStorage pra persistir entre abas e paginas
					// localStorage.setItem(this.localStorageId, JSON.stringify({ username: username, token: token }));

					// Retorna true pra indicar sucesso
					return true;
				} else {
					// Retorna falso pra indicar falso login
					return false;
				}
			});
	}

	tratarErroJWT = (response) => {
		const error = response.json()
		switch (error['error_type']) {
			case "TokenExpiredError": {
				this.router.navigate(['/auth/login'], { queryParams: this.route.snapshot.queryParams })
				break;
			}
		}
	}
	redirectSetToken() {
		window.location.href = this.sistema.endereco + '/auth/set-token' + '?redirect-url=' + this.redirectUrl + '&token=' + this.token;
	}
	redirect(url: string = this.redirectUrl) {
		window.location.href = this.sistema.endereco + '/' + url
	}
	redirectRemoveToken() {
		window.location.href = this.sistema.endereco + '/auth/remove-token' + '?redirect-url=' + this.redirectUrl
	}

	/** @todo remover esse método inútil */
	getUser(): Observable<Response> {
		const headers = new Headers({ 'Authorization': 'Bearer ' + this.token });
		const options = new RequestOptions({ headers: headers });
		const body = {
			sistemaKey: this.sistemaKey
		};
		// retorna o usuário logado ou false
		return this.http.post(this.ssoUrl + '/check-user', body, options)
			.map((response: Response) => response.json());
	}
	getSistema(): Observable<Response> {
		const headers = new Headers({ 'Authorization': 'Bearer ' + this.token });
		const options = new RequestOptions({ headers: headers });
		return this.http.get(this.ssoUrl + '/sso-auth/get-sistema/' + this.sistemaKey, options)
			.map((response: Response) => response.json());
	}
	logout(): void {
		// Limpa o token do localStorage e da memória
		this.removetoken()

		// pega a url do sistema para retornar
		this.getSistema().subscribe((sistema) => {
			this.sistema = sistema;
			if (this.redirectUrl) {
				this.redirectRemoveToken()
			}
		});

	}
	removetoken(): void {
		this.token = null;
		localStorage.removeItem(this.localStorageId);
	}

	ngOnDestroy(): void {
		this.routeSubscription.unsubscribe()
	}
}
