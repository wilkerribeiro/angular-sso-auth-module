import { Params, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
	selector: 'app-sso-auth-login-page',
	templateUrl: './login-page.component.html',
	styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {

	login: string;
	senha: string;

	constructor(public authenticationService: AuthService, private route: ActivatedRoute) { }

	teste() {
		console.log(this.authenticationService);

		this.authenticationService.login(this.login, this.senha)
			.subscribe(result => {
				if (result) {
					console.log('deu bom');
				} else {
					console.log('deu ruim');
				}
			});
	}

	decode() {
		this.authenticationService.getUser().subscribe((data) => {
			console.log('decoded ->', data);
		});
	}

}
