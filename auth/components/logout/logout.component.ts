import { Params, ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-sso-auth-logout',
	template: ``
})
export class LogoutComponent implements OnInit {
	constructor(
		private authService: AuthService,
		private router: Router,
		private route: ActivatedRoute
	) { }

	redirectUrl: string;
	ngOnInit() {
		this.authService.logout()
	}
}
