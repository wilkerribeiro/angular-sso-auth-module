import { Params, ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-sso-auth-check',
	template: ``
})
export class CheckComponent implements OnInit {
	constructor(
		private authService: AuthService,
		private router: Router,
		private route: ActivatedRoute
	) { }


	redirectToLogin(): void {
		this.router.navigate(['/auth/login'], {
			queryParams: this.route.snapshot.queryParams
		});
	}
	ngOnInit() {
		if (this.authService.token) {
			this.authService.validarToken().subscribe((response) => {
				if (response) {
					this.authService.redirectSetToken();
					return;
				}
				this.redirectToLogin()
			}, this.authService.tratarErroJWT);
		} else {
			this.redirectToLogin()
		}

	}
}
