import { CheckComponent } from './components/check/check.component';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { FormsModule } from '@angular/forms';
import { LogoutComponent } from './components/logout/logout.component'

@NgModule({
	providers: [
		AuthGuard
	],
	imports: [
		HttpModule,
		FormsModule,
		AuthRoutingModule
	],
	declarations: [CheckComponent, LoginPageComponent, LogoutComponent]
})
export class AuthModule { }
